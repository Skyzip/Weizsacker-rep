﻿//-------------------------------------------------------------------------------------------

Menük használata:

//*******************************************************************************************
- Fájl:
	- Periódusos rendszer:
		Megjeleníti a periódusos rendszert.
	
	- Visszaállítás:
		Visszaállítja az alapértelmezett pocíciót és
		a képernyő forgatását, továbbá a skálázást, a minőséget
		és a lépésközt az atommagok között.
	
	- Kilépés:
		Biztonságosan bezárja a programot.
*********************************************************************************************
//*******************************************************************************************
- Beállítások:
	- Gyorsítótár:
		Az adatokat betöltjük a memóriába és így rajzoljuk ki
		őket felgyorsítva evvel a rajzolást, ám ekkor az így
		kiszámolt atommagokon már nem változtathatunk.
	
	- Felugró ablakok letiltása:
		A gyorsítótár betöltése lassú lehet egyes számítógépeken,
		s ekkor ha a felugró ablakok nincsennek letiltva, lehetőségünk
		van a számítások befejezésére. (iszonyú régi gépeken ne legyen letiltva)
*********************************************************************************************
//*******************************************************************************************
- Megjelenítés:
	- Színes atomok:
		Színesek lesznek a megjelenített atommagok. Kissé lassulhat a sebesség.
	
	- Háttér:
		- Szín:
			Beállíthatjuk a háttérszínt (RGB vörös-zöld-kék komponenseit).

		- Határvonalak:
			Rácsokkal veszi körbe a grafikont, hogy a térhatás jobban
			érzékelhetővé váljon.
*********************************************************************************************
---------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

Vertikális csúszkák:

//*******************************************************************************************
- Bal:
	Skálázás mértékének beállítása.

- Középső:
	Gömbök minőségének beállítása. Minnél nagyobb a szám annál jobb a minőség.

- Jobb:
	Gömbök kirajzolásának lépésközének beállítása. Minnél kisebb annál több gömböt láthatunk.
	(pl. 3 esetén minden 3. atommagot rajzolja ki.)
*********************************************************************************************
---------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

Irányítás:

//*******************************************************************************************
	W___________
	Előre haladhatunk a térben. (aszerint, hogy merre nézünk)

	S___________
	Hátrafele haladhatunk a térben.

	A___________
	Balra haladhatunk a térben.

	D___________
	Jobbra haladhatunk a térben.

	Iránygombok__________
	Arrafele forgatja a kamera állását amerre a nyilakat nyomjuk.

	Space_______
	Felfele haladhatunk. (attól függetlenül, hogy merre nézünk)

	Ctrl________
	Lefele haladhatunk. (attól függetlenül, hogy merre nézünk)

	1-es 2-es illetve 3-as gombok__________
	Előre eltárazott metszeteket figyelhetjük meg.

	Egér_________
	A képernyőt forgathatjuk. A forgás középpontja az origo.
	Az ALT billenytű nyomvatartásával más síkokat forgathatunk.	
*********************************************************************************************
---------------------------------------------------------------------------------------------